#include <stm32f4xx.h>

void encoder_init(void) {
  
  // 1. Enable clock for the port where encoder is connected.
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
  
  // 2. Set used GPIO's as alternate function
  GPIOA->MODER = (GPIOA->MODER & ~(GPIO_MODER_MODE0_Msk)) | (GPIO_MODER_MODE0_1);
  GPIOA->MODER = (GPIOA->MODER & ~(GPIO_MODER_MODE1_Msk)) | (GPIO_MODER_MODE1_1);
  
  // 3. Choose proper alternate function connected witch timer (here TIM2_CH1 and TIM2_CH2)
  GPIOA->AFR[0] = (GPIOA->AFR[0] & ~(GPIO_AFRL_AFSEL0_Msk)) | (GPIO_AFRL_AFSEL0_0);
  GPIOA->AFR[0] = (GPIOA->AFR[0] & ~(GPIO_AFRL_AFSEL1_Msk)) | (GPIO_AFRL_AFSEL1_0);
  
  // 4. Enable clock for timer (here TIM2)
  RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
  
  // 5. Set Auto Reload Register value
  TIM2->ARR = 0xFFFFFFFF;
  
  // 6. Map Encoder to channels in timer settings
  TIM2->CCMR1 = (TIM2->CCMR1 & ~(TIM_CCMR1_CC1S_Msk)) | TIM_CCMR1_CC1S_0;  // channel configured as input, IC1 mapped on TI1
  TIM2->CCMR1 = (TIM2->CCMR1 & ~(TIM_CCMR1_CC2S_Msk)) | TIM_CCMR1_CC2S_0;  // channel configured as input, IC2 mapped on TI2
  
  // 7. Choose polarity of channels
  TIM2->CCER &= ~(TIM_CCER_CC1P);  // OC1 active high
  TIM2->CCER &= ~(TIM_CCER_CC1NP); // Both with above --> noninverted/rising edge
  
  TIM2->CCER &= ~(TIM_CCER_CC2P);
  TIM2->CCER &= ~(TIM_CCER_CC2NP);
  
  // 8. Choose filter
  TIM2->CCMR1 = (TIM2->CCMR1 & ~(TIM_CCMR1_IC1F_Msk)) | (TIM_CCMR1_IC1F_0 | TIM_CCMR1_IC1F_1 | TIM_CCMR1_IC1F_2 | TIM_CCMR1_IC1F_3);  
  TIM2->CCMR1 = (TIM2->CCMR1 & ~(TIM_CCMR1_IC2F_Msk)) | (TIM_CCMR1_IC2F_0 | TIM_CCMR1_IC2F_1 | TIM_CCMR1_IC2F_2 | TIM_CCMR1_IC2F_3);  
  
  // 9. Choose active edges (here both)
  TIM2->SMCR = (TIM2->SMCR & ~(TIM_SMCR_SMS_Msk)) | (TIM_SMCR_SMS_0 | TIM_SMCR_SMS_1);
  
  // 10. Enable timer
  TIM2->CR1 |= TIM_CR1_CEN;
}

int main(void) {
  encoder_init();
  
  // Observe TIM2->CNT for value
  // Observe TIM2->CR1:DIR for direction
  
  while(1);
}
